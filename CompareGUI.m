function varargout = CompareGUI(varargin)
% COMPAREGUI MATLAB code for CompareGUI.fig
%      COMPAREGUI, by itself, creates a new COMPAREGUI or raises the existing
%      singleton*.
%
%      H = COMPAREGUI returns the handle to a new COMPAREGUI or the handle to
%      the existing singleton*.
%
%      COMPAREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMPAREGUI.M with the given input arguments.
%
%      COMPAREGUI('Property','Value',...) creates a new COMPAREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CompareGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CompareGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CompareGUI

% Last Modified by GUIDE v2.5 04-Sep-2014 16:47:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CompareGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CompareGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CompareGUI is made visible.
function CompareGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CompareGUI (see VARARGIN)

% Choose default command line output for CompareGUI
handles.output = hObject;

path1 = varargin{2};
path2 = varargin{3};
ntimepoints = varargin{4};

handles.path1 = path1;
handles.path2 = path2;
handles.ntimepoints = ntimepoints;

slashind = find(path1=='/');
handles.subjectname = path1(slashind(1)+1:slashind(2)-1);
handles.conditionstr = path1(slashind(2)+1:slashind(3)-1);

handles.cond1str = path1(slashind(end-1)+1:end);
handles.cond1str(handles.cond1str == '/') = '-';

slashind = find(path2=='/');
handles.cond2str = path2(slashind(end-1)+1:end);
handles.cond2str(handles.cond2str == '/') = '-';

set(handles.lbl_cond1, 'String', handles.cond1str);
set(handles.lbl_cond2, 'String', handles.cond2str);

handles.dj = 1; %needs to be replaced by the actual desired joint
handles.datafull1 = loaddata(path1, ntimepoints);
handles.datafull2 = loaddata(path2, ntimepoints);

handles.shift = [0 0];
handles.currh = 1;


handles.useSynchronization = true;
handles.plotTrials = true;

handles = updatePlot(handles);

%find prediction scripts
fn = ls('*classification*');

set(handles.lb_algorithmstouse, 'String', fn);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CompareGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CompareGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_generateplot.
function btn_generateplot_Callback(hObject, eventdata, handles)
% hObject    handle to btn_generateplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
npoints = handles.ntimepoints;
d1 = handles.datafull1;
d2 = handles.datafull2;
max1 = max(d1(:));
max2 = max(d2(:));

npage1 = size(d1,3);
npage2 = size(d2,3);

[singletrialshifts1, singletrialshifts2] = getSingleTrialShifts(handles);

%determine which were selected
selectedJoints = get(handles.lb_jointstouse, 'value');
selectedJoints = selectedJoints{1};

sj_str = get(handles.lb_jointstouse, 'string');
sj_str = sj_str{2};
goodjoints = [];
for i = selectedJoints
   currJoint= strtrim(sj_str{i});
   if(strcmp(currJoint, 'ALL'))
       goodjoints = 1:size(d1, 2);
       break;
   end
   goodjoints(end+1) = str2double(currJoint);
end 

disp('Using joints:');
disp(goodjoints);

d1 = d1(:,goodjoints, :);
d2 = d2(:,goodjoints, :);

figure;
hold off;
%remove and append values to complete npoints time point window
for i = 1:size(d1,3)
    currdata = d1(:,:,i);
    xvals = 1:size(currdata,1);
    xvals = xvals + singletrialshifts1(i) + handles.shift(1);
    
    if( xvals(1) < 1 )
        nbo = sum(xvals<1);
        currdata(1:nbo,:) = [];
        currdata(end+1:end+nbo,:) = repmat(currdata(end,:), nbo, 1);
    elseif( xvals(end) > npoints )
        nao = sum(xvals>npoints);
        currdata(end-nao+1:end,:) = [];
        currdata = cat(1, repmat(currdata(1,:), nao, 1), currdata);
    end
    
    d1(:,:,i) = currdata;
    if(handles.plotTrials)
        try
            a=plot(currdata(:,handles.dj)/max1, 'g');
        catch error
            a=plot(currdata(:,1)/max1, 'g');
        end
    end
    hold on;
end

for i = 1:size(d2,3)
    currdata = d2(:,:,i);
    xvals = 1:size(currdata,1);
    xvals = xvals + singletrialshifts2(i) + handles.shift(2);
    
    if( xvals(1) < 1 )
        nbo = sum(xvals<1);
        currdata(1:nbo,:) = [];
        currdata(end+1:end+nbo,:) = repmat(currdata(end,:), nbo, 1);
    elseif( xvals(end) > npoints )
        nao = sum(xvals>npoints);
        currdata(end-nao+1:end,:) = [];
        currdata = cat(1, repmat(currdata(1,:), nao, 1), currdata);
    end
    
    d2(:,:,i) = currdata;
    if(handles.plotTrials)
        try
            b=plot(currdata(:,handles.dj)/max2, 'm');
        catch error
            b=plot(currdata(:,1)/max2, 'm');
        end
    end
    hold on;
    
end
if(handles.plotTrials)
    legplots = [a b];
    legdescrip = {handles.cond1str, handles.cond2str};
else
    legplots = [];
    legdescrip ={};
end

%perform classification on aligned and cropped datasets
sc = get(handles.lb_algorithmstouse, 'value');
sc_str = get(handles.lb_algorithmstouse, 'string');

colors = 'rbkyc';

for i = sc
   filename =  strtrim(sc_str(i, :));
   fname = filename(1:end-2);
   
   cmd = sprintf('%s(%s,%s, %d, %d)', fname, 'd1', 'd2', npage1, npage2);
   
   fprintf(cmd)
   fprintf('...\n');
   errors = eval(cmd);
   
   currcolor = colors(mod(i, length(colors))+1);
   legplots(end+1) = plot(errors, currcolor, 'linewidth', 2.5);
   legdescrip{end+1} = fname(6:8);
   
   cmd = cat(2, legdescrip{end}, '=', 'errors;'); 
   eval(cmd);
end
fprintf('done\n');
legend(legplots, legdescrip);
title(cat(2,handles.subjectname, ' ', handles.conditionstr));
xlim([1 npoints]);
ylim([0 1]);

warning('off','all');
choice = questdlg('Save classification errors?', ...
	'prompt', ...
	'yes','no','');
warning('on','all');

if(strcmp(choice, 'yes'))

    if(~exist('Error Time Series', 'dir'))
        mkdir('Error Time Series');
    end

    subjfolder = cat(2, 'Error Time Series\', handles.subjectname);
    if(~exist(subjfolder, 'dir'))
        mkdir(subjfolder);
    end

    condfolder = cat(2, subjfolder,'\',handles.conditionstr);
    if(~exist(condfolder, 'dir'))
        mkdir(condfolder);
    end
    savename = cat(2, condfolder,'\', handles.cond1str,' vs ', handles.cond2str);
    save(savename, legdescrip{end-length(sc)+1:end});
    
end


% --- Executes on selection change in lb_algorithmstouse.
function lb_algorithmstouse_Callback(hObject, eventdata, handles)
% hObject    handle to lb_algorithmstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_algorithmstouse contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_algorithmstouse


% --- Executes during object creation, after setting all properties.
function lb_algorithmstouse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_algorithmstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lb_jointstouse.
function lb_jointstouse_Callback(hObject, eventdata, handles)
% hObject    handle to lb_jointstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_jointstouse contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_jointstouse


% --- Executes during object creation, after setting all properties.
function lb_jointstouse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_jointstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_aligncondition1.
function btn_aligncondition1_Callback(hObject, eventdata, handles)
% hObject    handle to btn_aligncondition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.currh = 1;
guidata(hObject, handles);

% --- Executes on button press in btn_aligncondition2.
function btn_aligncondition2_Callback(hObject, eventdata, handles)
% hObject    handle to btn_aligncondition2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.currh = 2;
guidata(hObject, handles);

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in lb_jointtoplot.
function lb_jointtoplot_Callback(hObject, eventdata, handles)
% hObject    handle to lb_jointtoplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_jointtoplot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_jointtoplot
handles.dj = get(handles.lb_jointtoplot, 'value');
handles = updatePlot(handles);
xlim([1 handles.ntimepoints]);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lb_jointtoplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_jointtoplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function datafull = loaddata(path, ntimepoints)

str = sprintf('%s/*.dat', path);
files = ls(str);

datafull = zeros(ntimepoints, 12, size(files,1));
hold off;
for i = 1:size(files,1)
    
    folder = sprintf('%s/',path);
    data = load([folder files(i,:)]);
    
    %remove unnecessary joint angles from the analysis
    
    data(:,22:24)=[];data(:,20)=[]; data(:,16)=[]; data(:,12)=[];
    data(:,8:9)=[]; data(:,3:5)=[]; data(:, 1) = [];
    
    datafull(:,:,i) = data(1:ntimepoints,:);
    
end


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
    ca = axis;
    
    if(handles.currh == 1)
        currh = handles.h1; %vector of handles, handles.currh is either 1 or 2
    else
        currh = handles.h2;
    end

    if(strcmp(eventdata.Key, 'rightarrow'))
        for i = 1:length(currh)
            xvals = get(currh(i), 'xdata');
            set(currh(i), 'xdata', xvals+1);
        end
        handles.shift(handles.currh) = handles.shift(handles.currh) + 1;
    elseif(strcmp(eventdata.Key, 'leftarrow'))
        for i = 1:length(currh)
            xvals = get(currh(i), 'xdata');
            set(currh(i), 'xdata', xvals-1);
        end
        handles.shift(handles.currh) = handles.shift(handles.currh) - 1;
    end
    axis(ca);
  guidata(hObject, handles);

  function handles = updatePlot(handles)
    npage1 = size(handles.datafull1,3);
    npage2 = size(handles.datafull2, 3);
    handles.h1 = zeros(npage1, 1);
    handles.h2 = zeros(npage2, 1);
   
    hold off;
    
    [singletrialshifts1, singletrialshifts2] = getSingleTrialShifts(handles);
    for k = 1:npage1
        currdata = handles.datafull1(:, handles.dj, k);
        xvals = 1:size(currdata,1);
        handles.h1(k) = plot(xvals +singletrialshifts1(k)+handles.shift(1), currdata , 'g');
        hold on;
    end

    for k = 1:npage2
        currdata = handles.datafull2(:, handles.dj, k);
        xvals = 1:size(currdata,1);
        handles.h2(k) = plot(xvals + singletrialshifts2(k) + handles.shift(2), currdata , 'm');
        hold on;
    end
    title(cat(2,handles.subjectname, ' ', handles.conditionstr));
    xlim([1 handles.ntimepoints]);

% --- Executes on button press in cb_usesynchronization.
function cb_usesynchronization_Callback(hObject, eventdata, handles)
% hObject    handle to cb_usesynchronization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_usesynchronization
flag = get(hObject, 'value');
if(flag == 1)
    handles.useSynchronization = true;
else
    handles.useSynchronization = false;
end
guidata(hObject, handles)

% --- Executes on button press in cb_plottrials.
function cb_plottrials_Callback(hObject, eventdata, handles)
% hObject    handle to cb_plottrials (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plottrials
flag = get(hObject, 'value');
if(flag == 1)
    handles.plotTrials= true;
else
    handles.plotTrials= false;
end
guidata(hObject, handles)


function [singletrialshifts1, singletrialshifts2] = getSingleTrialShifts(handles)
    
    d1 = handles.datafull1;
    d2 = handles.datafull2;

    filename1 = handles.path1;
    filename1(filename1 == '/') = '-';
    path1 = cat(2, 'Alignments/',filename1,'.mat');

    filename2 = handles.path2;
    filename2(filename2 == '/') = '-';
    path2 = cat(2, 'Alignments/',filename2,'.mat');

    npage1 = size(d1,3);
    npage2 = size(d2,3);
    if(handles.useSynchronization)
        if(~exist(path1, 'file'))
            singletrialshifts1 = zeros(npage1, 1);
        else
            singletrialshifts1 = load(path1);
            singletrialshifts1 = singletrialshifts1.currshift;
        end

        if(~exist(path2, 'file'))
            singletrialshifts2 = zeros(npage2, 1);
        else
            singletrialshifts2 = load(path2);
            singletrialshifts2 = singletrialshifts2.currshift;
        end
    else
        singletrialshifts1 = zeros(npage1, 1);
        singletrialshifts2 = zeros(npage2, 1);
    end


% --- Executes on selection change in lb_jointstouse.
function listbox4_Callback(hObject, eventdata, handles)
% hObject    handle to lb_jointstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_jointstouse contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_jointstouse


% --- Executes during object creation, after setting all properties.
function listbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_jointstouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
