function varargout = SynchronizationGUI(varargin)
% SYNCHRONIZATIONGUI MATLAB code for SynchronizationGUI.fig
%      SYNCHRONIZATIONGUI, by itself, creates a new SYNCHRONIZATIONGUI or raises the existing
%      singleton*.
%
%      H = SYNCHRONIZATIONGUI returns the handle to a new SYNCHRONIZATIONGUI or the handle to
%      the existing singleton*.
%
%      SYNCHRONIZATIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SYNCHRONIZATIONGUI.M with the given input arguments.
%
%      SYNCHRONIZATIONGUI('Property','Value',...) creates a new SYNCHRONIZATIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SynchronizationGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SynchronizationGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SynchronizationGUI

% Last Modified by GUIDE v2.5 04-Sep-2014 21:45:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SynchronizationGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SynchronizationGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SynchronizationGUI is made visible.
function SynchronizationGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SynchronizationGUI (see VARARGIN)

handles = updateSelection(hObject,handles);

handles.dj = 1;
% Choose default command line output for SynchronizationGUI
handles.output = hObject;
handles.ntimepoints = str2num(get(handles.numberoftimepoints, 'string'));
handles.state = 2;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SynchronizationGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SynchronizationGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in lb_subjectname.
function lb_subjectname_Callback(hObject, eventdata, handles)
% hObject    handle to lb_subjectname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_subjectname contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_subjectname
handles = updateSelection(hObject,handles);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lb_subjectname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_subjectname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lb_condition.
function lb_condition_Callback(hObject, eventdata, handles)
% hObject    handle to lb_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_condition contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_condition
handles = updateSelection(hObject,handles);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lb_condition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lb_initialpose.
function lb_initialpose_Callback(hObject, eventdata, handles)
% hObject    handle to lb_initialpose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_initialpose contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_initialpose
handles = updateSelection(hObject,handles);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lb_initialpose_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_initialpose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lb_finalpose.
function lb_finalpose_Callback(hObject, eventdata, handles)
% hObject    handle to lb_finalpose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_finalpose contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_finalpose
handles = updateSelection(hObject,handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function lb_finalpose_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_finalpose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function txt_rootfolder_Callback(hObject, eventdata, handles)
% hObject    handle to txt_rootfolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_rootfolder as text
%        str2double(get(hObject,'String')) returns contents of txt_rootfolder as a double


% --- Executes during object creation, after setting all properties.
function txt_rootfolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_rootfolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_selectregiontosynchronize.
function handles = btn_selectregiontosynchronize_Callback(hObject, eventdata, handles)
% hObject    handle to btn_selectregiontosynchronize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1)
log(handles, 'click twice on plot to define which region of the mean is used for filter');
interval = ginput(2);

i1 = round(interval(1,1));
i2 = round(interval(2,1));
handles.interval1 = i1;
handles.interval2 = i2;


try
delete(handles.regionh)
catch error
end

ca = axis();
h = fill([i1 i1 i2 i2], [ca(3) ca(4) ca(4) ca(3)],'b','FaceAlpha',.2,'EdgeColor','none');
handles.regionh = h;

axis(ca);
uistack(h, 'bottom')

log(handles, sprintf('Current interval: %d to %d', i1, i2));
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
function handles = updateSelection(hObject,handles)
    %switch to make sure that listbox's dont throw erorr upon selection
    switch(get(hObject, 'tag'))
        
        case 'lb_subjectname'
           set(handles.lb_condition, 'value', 1);
           set(handles.lb_initialpose, 'value', 1);
           set(handles.lb_finalpose, 'value', 1);
        case 'lb_condition'
           set(handles.lb_initialpose, 'value', 1);
           set(handles.lb_finalpose, 'value', 1);
        case 'lb_initialpose'
           set(handles.lb_finalpose, 'value', 1);
           
    end
    
    handles.ntimepoints = str2num(get(handles.numberoftimepoints, 'string'));
    
    handles.rootfolder = get(handles.txt_rootfolder, 'String');
    handles.path = handles.rootfolder;

    subjectfolders = ls(handles.rootfolder);
    subjectfolders = subjectfolders(3:end,:);
    set(handles.lb_subjectname, 'String', subjectfolders);

    selection = get(handles.lb_subjectname, 'Value');
    subjectfolders = get(handles.lb_subjectname, 'String');
    selectedSubject = strtrim(subjectfolders(selection, :));

    handles.path = cat(2, handles.path,'/' ,selectedSubject);

    conditions = ls(handles.path);
    conditions = conditions(3:end, :);
    markedrows = [];
    for i = 1:size(conditions, 1) 
        currCond = strtrim(conditions(i, :));
        pathtmp = cat(2, handles.path, '/', currCond);

        if(~isdir(pathtmp) || strcmp(currCond, [selectedSubject '_log']) || strcmp(currCond, [selectedSubject 'train']))
            markedrows(end+1) = i;
        end 
    end
    conditions(markedrows,:) = [];
    set(handles.lb_condition, 'String', conditions);
    
    selection = get(handles.lb_condition, 'Value');
    conditions = get(handles.lb_condition, 'String');
    selectedCondition = strtrim(conditions(selection, :));

    handles.path = cat(2, handles.path,'/' , selectedCondition);

    initialposes = ls(handles.path);
    initialposes = initialposes(3:end, :);
    markedrows = [];
    for i = 1:size(initialposes, 1) 
        currCond = strtrim(initialposes(i, :));
        pathtmp = cat(2, handles.path, '/', currCond);

        if(~isdir(pathtmp))
            markedrows(end+1) = i;
        end 
    end
    initialposes(markedrows,:) = [];
    set(handles.lb_initialpose, 'String', initialposes);

    selection = get(handles.lb_initialpose, 'Value');
    initialposes = get(handles.lb_initialpose, 'String');
    selectedinitialpose = strtrim(initialposes(selection, :));

    handles.path = cat(2, handles.path,'/' , selectedinitialpose);

    finalposes = ls(handles.path);
    finalposes = finalposes(3:end, :);
    markedrows = [];
    for i = 1:size(finalposes, 1) 
        currCond = strtrim(finalposes(i, :));
        pathtmp = cat(2, handles.path, '/', currCond);

        if(~isdir(pathtmp))
            markedrows(end+1) = i;
        end 
    end
    finalposes(markedrows,:) = [];
    set(handles.lb_finalpose, 'String', finalposes);
    
    selection = get(handles.lb_finalpose, 'Value');
    finalposes = get(handles.lb_finalpose, 'String');
    selectedfinalpose = strtrim(finalposes(selection, :));

    handles.path = cat(2, handles.path,'/' , selectedfinalpose);



% --- Executes on button press in btn_plotdata.
function btn_plotdata_Callback(hObject, eventdata, handles)
% hObject    handle to btn_plotdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = updateSelection(hObject,handles);
handles.currentval = 0;

if(isfield(handles, 'prevh'))
   handles=rmfield(handles, 'prevh'); 
end

if(isfield(handles, 'h'))
   handles=rmfield(handles, 'h'); 
   handles=rmfield(handles, 'shift'); 
end

str = sprintf('%s/*.dat', handles.path);
files = ls(str);

handles.datafull = zeros(handles.ntimepoints, 12, size(files,1));
axes(handles.axes1)
hold off;
handles.trialfiles = {};
for i = 1:size(files,1)
    
    folder = sprintf('%s/',handles.path);
    data = load([folder files(i,:)]);
    handles.trialfiles{end+1} = [folder files(i,:)];
    %remove unnecessary joint angles from the analysis
    
    data(:,22:24)=[];data(:,20)=[]; data(:,16)=[]; data(:,12)=[];
    data(:,8:9)=[]; data(:,3:5)=[]; data(:, 1) = [];
    
    handles.datafull(:,:,i) = data(1:handles.ntimepoints,:);
    
    plot(handles.datafull(:,handles.dj, i), 'm')
    hold on;
end
a=plot(mean(handles.datafull(:,handles.dj,:), 3),'m', 'linewidth', 3);
legend(a, 'mean');
guidata(hObject, handles);

function numberoftimepoints_Callback(hObject, eventdata, handles)
% hObject    handle to numberoftimepoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numberoftimepoints as text
%        str2double(get(hObject,'String')) returns contents of numberoftimepoints as a double


% --- Executes during object creation, after setting all properties.
function numberoftimepoints_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numberoftimepoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lb_joinindexforsynchronization.
function lb_joinindexforsynchronization_Callback(hObject, eventdata, handles)
% hObject    handle to lb_joinindexforsynchronization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lb_joinindexforsynchronization contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lb_joinindexforsynchronization
handles.dj = get(hObject, 'Value');
log(handles, sprintf('Joint index changed to %d', handles.dj));

guidata(hObject, handles);
btn_plotdata_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function lb_joinindexforsynchronization_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb_joinindexforsynchronization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function log(handles,msg)
    set(handles.txt_debug, 'String', msg)


function [newdata, cumshifts]= woody(olddata, dj, iterations, i1, i2)
%oldata - 2d matrix, timepoints x joints x trials
%dj - desired joint index for plotting
%iterations number of times woody filter should be reapplied
    cumshifts = zeros(size(olddata, 3),1)';
    for i = 1:iterations
        [olddata, shifts] = synchronize(olddata,size(olddata,1),dj, i1,i2);
        cumshifts = cumshifts + shifts;
    end
    newdata=olddata;

function [newdata, shifts] = synchronize(olddata, ntimepoints,dj,i1,i2)

    avg = squeeze(mean(olddata(:,dj,:),3)); 
    newdata = zeros(size(olddata));
    shifts = [];
    for i = 1:size(olddata, 3)
        currTrace = olddata(:, dj, i);

        [c, lags] = aperiodicXCorr(avg, currTrace, i1:i2, -20:50);
        [~, index] = max(c);
        
        shifts(end+1) = lags(index);
        sampleaxis = (1:length(currTrace))-lags(index);
        trace = currTrace(sampleaxis>0 & sampleaxis<=ntimepoints)';
        for j = 1:size(olddata, 2)
            if(sampleaxis(1) <= 0)
                newdata(:,j,i) = [trace, ones(1, sum(sampleaxis<=0))*trace(end)];
            else
                newdata(:,j,i) = [ones(1, sampleaxis(1)-1)*trace(1), trace];
            end
        end
    end
    
function [c, lagtimes] = aperiodicXCorr(s1, s2, intervalToCorrelate, lagtimes)

snip1 = s1(intervalToCorrelate);
c = zeros(1, length(lagtimes));
for i = 1:length(lagtimes)
    try
        snip2 = s2(lagtimes(i)+intervalToCorrelate);
    catch error
        return
    end
    matc = cov(snip1, snip2);
    c(i) = matc(2,1);
end
    

function txt_debug_Callback(hObject, eventdata, handles)
% hObject    handle to txt_debug (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_debug as text
%        str2double(get(hObject,'String')) returns contents of txt_debug as a double


% --- Executes during object creation, after setting all properties.
function txt_debug_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_debug (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_woodyfilterdata.
function btn_woodyfilterdata_Callback(hObject, eventdata, handles)
% hObject    handle to btn_woodyfilterdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = btn_selectregiontosynchronize_Callback(hObject, eventdata,handles);
pause(2);
[output, handles.shift] = woody(handles.datafull,handles.dj, 15, handles.interval1, handles.interval2);
handles.shift = -handles.shift';
try
delete(handles.regionh)
catch error
end

axes(handles.axes1)
hold on;

for i = 1:size(output, 3)
   plot(output(:, handles.dj, i), 'b')
end
plot(mean(output(:,handles.dj,:),3), 'b', 'linewidth',3);

str = 'Woody filter lag times: ';
for i = 1:length(handles.shift)
   str = cat(2, str, num2str(handles.shift(i)), ' '); 
end

log(handles, str);
guidata(hObject, handles);

% --- Executes on button press in manuallyaligndata.
function manuallyaligndata_Callback(hObject, eventdata, handles)
% hObject    handle to manuallyaligndata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
btn_plotdata_Callback(hObject, eventdata, handles)
handles.currentval = 0;

if(isfield(handles, 'prevh'))
   handles=rmfield(handles, 'prevh'); 
end

if(isfield(handles, 'h'))
   handles=rmfield(handles, 'h'); 
   handles=rmfield(handles, 'shift'); 
end

log(handles, 'Use up down/arrow keys to select which trial to shift, left/right arrows to shift');

guidata(hObject, handles);

% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

switch(handles.state)
    case 2
        if(~isfield(handles, 'h'))
            handles.h = get(handles.axes1,'Children');
            handles.h = handles.h(end:-1:2);
            handles.shift = zeros(size(handles.h));
        end
        
        if(isfield(handles, 'prevh'))
           set(handles.prevh, 'color', 'm', 'linewidth', 1); 
        end
        
         if(strcmp(eventdata.Key, 'uparrow'))
            if(handles.currentval < length(handles.h))
                handles.currentval = handles.currentval + 1; 
            else    
                handles.currentval = 1;
            end
        elseif(strcmp(eventdata.Key, 'downarrow'))
            if(handles.currentval > 1)
                handles.currentval = handles.currentval - 1; 
            else    
                handles.currentval = length(handles.h);
            end
        end
        
        currh = handles.h(handles.currentval);
        currshift = handles.shift(handles.currentval);
        
        handles.prevh = currh;
        uistack(currh, 'top')
        set(currh, 'color', 'k', 'linewidth', 2);
        
        ca = axis;
        
        if(strcmp(eventdata.Key, 'rightarrow'))
            xvals = get(currh, 'xdata');
            currshift = currshift + 1;
            set(currh, 'xdata', xvals+1);
            axis(ca);
        elseif(strcmp(eventdata.Key, 'leftarrow'))
            xvals = get(currh, 'xdata');
            currshift = currshift - 1;
            set(currh, 'xdata', xvals-1); 
            axis(ca);
        end
        handles.shift(handles.currentval) = currshift;
        log(handles, cat(2, 'Current trial selected: ', num2str(handles.currentval),', Filename: ',  handles.trialfiles{handles.currentval}, ', Current shift: ' , num2str(handles.shift(handles.currentval))));
end

guidata(hObject, handles);


% --- Executes on button press in btn_savealignment.
function btn_savealignment_Callback(hObject, eventdata, handles)
% hObject    handle to btn_savealignment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = handles.path;
if(~exist('Alignments', 'dir'))
    mkdir('Alignments', 'dir');
end
filename(filename == '/') = '-';

path = cat(2, 'Alignments/',filename);

currshift=handles.shift;
save(path, 'currshift');
log(handles, sprintf('Lags were saved in %s.mat in the Alignment folder', filename));


% --- Executes on button press in btn_loadpreviouslags.
function btn_loadpreviouslags_Callback(hObject, eventdata, handles)
% hObject    handle to btn_loadpreviouslags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = handles.path;
filename(filename == '/') = '-';
path = cat(2, 'Alignments/',filename,'.mat');
if(exist(path, 'file'))
    shifts = load(path);
    shifts = shifts.currshift;
else
    log(handles, 'no mat file for those conditions detected');
    return
end
ca = axis;
str = 'Loaded Lag Values: ';
for i = 1:size(handles.datafull,3)
    currTrace = handles.datafull(:,handles.dj, i);
    sampleaxis = 1:length(currTrace);
    hold on
    plot(sampleaxis + shifts(i), currTrace);
    
    str = cat(2, str, num2str(shifts(i)), ' ');
end
log(handles, str);
axis(ca);

% --- Executes on button press in btn_absorrelthresholding.
function btn_absorrelthresholding_Callback(hObject, eventdata, handles)
% hObject    handle to btn_absorrelthresholding (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
btn_plotdata_Callback(hObject, eventdata, handles);
warning('off', 'all');
choice = questdlg('Choose a method', ...
	'prompt', ...
	'Absolute Threshold','Relative Threshold','');
warning('on', 'all');

switch(choice) 
    case 'Absolute Threshold'
        
        handles = btn_selectregiontosynchronize_Callback(hObject, eventdata, handles);
        
        ca = axis();
        
        prompt = {'Enter sample index where you would like to place the detected threshold'};
        dlg_title = 'Input';
        num_lines = 1;
        def = {'20'};
        answer = inputdlg(prompt,dlg_title,num_lines,def); 
        syncindex = str2num(answer{1});
        
        h=plot([syncindex syncindex], [ca(3) ca(4)], 'k');
        uistack(h, 'bottom');
        
        prompt = {'Enter absolute value used for synchronization:'};
        dlg_title = 'Input';
        num_lines = 1;
        def = {'20'};
        answer = inputdlg(prompt,dlg_title,num_lines,def); 
        offset = str2num(answer{1});
        
        handles.shift = zeros(size(handles.datafull, 3), 1);
        str = 'Abs Threshold Lags: ';
        
        btn_plotdata_Callback(hObject, eventdata, handles);
        
        for i = 1:size(handles.datafull,3)
            
            currTrace = handles.datafull(:, handles.dj, i);
            currTrace = currTrace(handles.interval1:handles.interval2);
            bin = (currTrace) <= offset;
            I = find(bin);
           
            try
                if(I(1) == 1)
                    I = I(end) + handles.interval1-1;
                else
                    I = I(1) + handles.interval1-1;
                end
            catch error
               log(handles, 'ERROR: Choose a smaller value'); 
               return
            end
             handles.shift(i) = syncindex - I;
            str = cat(2, str, num2str(handles.shift(i)), ' ');
            hold on
            sampleaxis = 1:size(handles.datafull, 1);
            plot(sampleaxis + handles.shift(i), handles.datafull(:, handles.dj,i));
            
        end
        
        axis(ca);
        log(handles, str);
        
    case 'Relative Threshold'
        
        log(handles, 'Select a baseline value (i.e. a point near beginning where a data approximately flatlines)')
        baseline = ginput(1);
        baseline = round(baseline(1));
        log(handles, cat(2,'Baseline Time: ', num2str(baseline)));
        
        ca = axis();
        
        handles = btn_selectregiontosynchronize_Callback(hObject, eventdata, handles);
        
        prompt = {'Enter sample index where you would like to place the detected threshold'};
        dlg_title = 'Input';
        num_lines = 1;
        def = {'20'};
        answer = inputdlg(prompt,dlg_title,num_lines,def); 
        syncindex = str2num(answer{1});
        
        h=plot([syncindex syncindex], [ca(3) ca(4)], 'k');
        uistack(h, 'bottom');
        
        prompt = {'Enter value relative to baseline used for synchronization:'};
        dlg_title = 'Input';
        num_lines = 1;
        def = {'-20'};
        answer = inputdlg(prompt,dlg_title,num_lines,def); 
        offset = str2num(answer{1});
        
        
        handles.shift = zeros(size(handles.datafull, 3), 1);
        str = 'Rel Treshold Lags: ';
        
        for i = 1:size(handles.datafull,3)
            
            currTrace = handles.datafull(:, handles.dj, i);
            currTrace = currTrace(handles.interval1:handles.interval2);
            bin =  (currTrace - currTrace(baseline)) <= offset;
            I = find(bin);
            try
                if(I(1) == 1)
                    I = I(end) + handles.interval1-1;
                else
                    I = I(1) + handles.interval1-1;
                end
            catch error
                
               log(handles, 'ERROR: Choose a smaller offset'); 
               return
            end
            
            handles.shift(i) = syncindex - I;
            str = cat(2, str, num2str(handles.shift(i)), ' ');
            hold on
            sampleaxis = 1:size(handles.datafull, 1);
            plot(sampleaxis + handles.shift(i), handles.datafull(:, handles.dj,i));
            
        end
        
        axis(ca);
        log(handles, str);
        
end

guidata(hObject, handles);


% --- Executes on button press in btn_loadcondition1.
function btn_loadcondition1_Callback(hObject, eventdata, handles)
% hObject    handle to btn_loadcondition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.condition1path = handles.path;
set(handles.txt_condition1, 'String', handles.path);
log(handles, cat(2, 'Condition 1 set to ', handles.path));
guidata(hObject, handles);

% --- Executes on button press in btn_loadcondition2.
function btn_loadcondition2_Callback(hObject, eventdata, handles)
% hObject    handle to btn_loadcondition2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.condition2path = handles.path;
set(handles.txt_condition2, 'String', handles.path);
log(handles, cat(2, 'Condition 2 set to ', handles.path));
guidata(hObject, handles);

function txt_condition1_Callback(hObject, eventdata, handles)
% hObject    handle to txt_condition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_condition1 as text
%        str2double(get(hObject,'String')) returns contents of txt_condition1 as a double


% --- Executes during object creation, after setting all properties.
function txt_condition1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_condition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_condition2_Callback(hObject, eventdata, handles)
% hObject    handle to txt_condition2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_condition2 as text
%        str2double(get(hObject,'String')) returns contents of txt_condition2 as a double


% --- Executes during object creation, after setting all properties.
function txt_condition2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_condition2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btn_compare.
function btn_compare_Callback(hObject, eventdata, handles)
% hObject    handle to btn_compare (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(isfield(handles, 'condition1path') && isfield(handles, 'condition2path'))
    CompareGUI(@disp,handles.condition1path, handles.condition2path, handles.ntimepoints);
else
   log(handles, 'The two conditions have not been specified'); 
end


% --- Executes on button press in btn_rejectcurrenselectedtrial.
function btn_rejectcurrenselectedtrial_Callback(hObject, eventdata, handles)
% hObject    handle to btn_rejectcurrenselectedtrial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
warning('off','all');
choice = questdlg('Delete trial?', ...
	'prompt', ...
	'yes','no','');
warning('on','all');

if(strcmp(choice, 'yes'))
    f = handles.trialfiles{handles.currentval};
    movefile(f, [f, '-del']);
    filename = handles.path;
    filename(filename == '/') = '-';

    path = cat(2, 'Alignments/',filename,'.mat');

    if( exist(path, 'file'))
       delete(path);
    end

end
log(handles, ['Deleted trial ' handles.currentval '. Alignment file was deleted (if it existed).']);
btn_plotdata_Callback(hObject, eventdata, handles);
