%data = time points x joint x trial, for both conditions that you are
%comparing
function Errors=LinDiscrimTMS(data,NofTrials, varargin)
%This function calculates the classification errors 
%using the built-in LDA classify function 
%CLASS = CLASSIFY(SAMPLE,TRAINING,GROUP)
Class=[];C=[];Ctemp=[];
%if(length(objs)==4) NofTrials(1)=NofTrials(1)+NofTrials(2);   NofTrials(2)=NofTrials(3)+NofTrials(4);
%end
% data(:,1,:)=[]; 
for k=1:size(data,3) %iterate over trials
	C=[];
    for i=1:size(data,1) %iterate over time points for each given trial k
        Sample=data(i,:,k);
        for u=1:size(data,3) %get all of the trial values  for this time point, store the observations in Training
            Training(u,:)=data(i,:,u);
        end
        Training(k,:)=[];   %remove the test observation
        
        if(k<=NofTrials(1)) %to check!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            Group=[ones(1,NofTrials(1)-1),ones(1,NofTrials(2))+1]; %vectors of ones and twos
        else
            
            Group=[ones(1,NofTrials(1)),ones(1,NofTrials(2)-1)+1];
        end
% Group=1:1:(2*NofTrials(1)-1);
% Group=Group';
%         k
%         i
        if(nargin < 3)
            Ctemp= classify(Sample,Training,Group);
            C=[C;Ctemp];
        else
            if(strcmp(varargin{1}, 'SVM'))
                SVMSTRUCT = svmtrain(Training, Group); %default c of 1;
                Ctemp = svmclassify(SVMSTRUCT, Sample);
                C=[C;Ctemp];
            else
                Ctemp= classify(Sample,Training,Group);
                C=[C;Ctemp];
            end
        end
    end 
    Class=[Class,C];
end
Group=[ones(1,NofTrials(1)),ones(1,NofTrials(2))+1];
for i=1:size(Class,2) %index for trials
    for k=1:size(Class,1) %index for samples
        if(Class(k,i)==Group(i))
            Class(k,i)=1;
        elseif(Class(k,i)~=Group(i))
            Class(k,i)=0;
        end
    end 
end 
Errors=[];
for i=1:size(Class,1)
    Errors(i)=(size(Class,2)-sum(Class(i,:)))/size(Class,2);
end