function error = dtwTMS(d1,d2)
    fulldata = cat(3,d1,d2);
    Class = [ones(1, size(d1, 3)), 1+ones(1, size(d2, 3))]; %vectors of 1's and 2's
    
    total = length(Class);
    misclassified = 0;
    for k = 1:size(fulldata,3)
        Sample = fulldata(:,:, k);
        
        for u=1:size(fulldata,3) %get all of the trial values  for this time point, store the observations in Training
            Training(:,:,u)=fulldata(:,:,u);
        end
        
        Training(:,:,k) = [];
        
        TrainingClass = Class;
        TrainingClass(k) = [];
        
        CorrectClass = Class(k);
        
        c1=0; c2=0;
        for u=1:size(Training,3) %get all of the trial values  for this time point, store the observations in Training
            currTrain =   Training(:,:,u);
            
            if(TrainingClass(u) == 1)
               c1 = c1 + dtw_c(Sample, currTrain, 15); 
            else
               c2 = c2 + dtw_c(Sample, currTrain, 15); 
            end
            
        end
        
        c1 = c1/sum(TrainingClass==1);
        c2 = c2/sum(TrainingClass==2);
        
        if(c1 < c2)
           predictedClass = 1;
        else
           predictedClass = 2;
        end
        
        if(predictedClass ~= CorrectClass)
           misclassified = misclassified +1;
        end
        
    end
    
    error = misclassified/total;
end