function [errors] = Sync_DTW_Classification(d1,d2,nt1,nt2)

    mtp = min([size(d1,1) size(d2,1)]);
    errors = zeros(1, mtp);
    for i = 1:mtp
        d1tmp = d1(1:i, :,:);
        d2tmp = d2(1:i, :,:);
        
        errors(i) = dtwTMS(d1tmp, d2tmp);
    end
        [b,a]=butter(2,1/10);
        errors=filtfilt(b,a,errors);
end